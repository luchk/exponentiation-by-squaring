#include <iostream>

unsigned long long int binPowRec (int a, int n)  // recursive (рекурсивний варіант)
{
	if (n == 0)
		return 1;
	if (n % 2 == 1)
		return binPowRec (a, n-1) * a;
	else 
	{
		int b = binPowRec (a, n/2);
		return b * b;
	}
}

unsigned long long int binPowNoRec (int a, int n)  // no recursive (не рекурсивний)
{
	int res = 1;
	while (n)
		if (n & 1) 
		{
			res *= a;
			--n;
		}
		else 
		{
			a *= a;
			n >>= 1;
		}
	return res;
}

unsigned long long int binPowNoRecOpt (int a, int n) // np recursive optimized. I found this solution in the Internet.  
{													//(не рекурсивний оптимізований. Знайшов це рішення на просторах 
	int res = 1;								   // інтернету)
	while (n) 
	{
		if (n & 1)
			res *= a;
		a *= a;
		n >>= 1;
	}
	return res;
}

int main()
{
	std::cout << "3 to power of 18" 	<< std::endl;
    std::cout << binPowRec(3, 18) 		<< std::endl;
    std::cout << binPowNoRec(3, 18) 	<< std::endl;
    std::cout << binPowNoRecOpt(3, 18) 	<< std::endl;
    return 0;
}